import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';

import { HeaderItem } from '../../Components';

function AboutTaskScreen({ navigation }) {
    return (
        <TouchableWithoutFeedback onPress={() => navigation.navigate('Home')}>
            <View style={{ flex: 1 }}>
                <HeaderItem title={'About Task'} />
                <View style={styles.wrapper}>
                    <Text style={styles.textStyle}>The task for you is to implement this design using react native.</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 7,
        marginTop: 16,
        paddingHorizontal: 16
    },
    textStyle: {
        fontSize: 18
    }
});

export default AboutTaskScreen;
