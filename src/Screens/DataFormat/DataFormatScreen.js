import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';

import { HeaderItem } from '../../Components';

function DataFormatScreen({ navigation }) {
    return (
        <TouchableWithoutFeedback onPress={() => navigation.navigate('Home')}>
            <View style={{ flex: 1 }}>
                <HeaderItem title={'Data Format'} />
                <View style={styles.wrapper}>
                    <Text style={styles.textStyle}>The data/content to be displayed will be inside the json file.</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 7,
        marginTop: 16,
        paddingHorizontal: 16
    },
    textStyle: {
        fontSize: 18
    }
});

export default DataFormatScreen;
