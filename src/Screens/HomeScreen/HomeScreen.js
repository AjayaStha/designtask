import React from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    Image,
} from 'react-native';

import { MenuItem } from '../../Components';

const DATA = [
    {
        id: 1,
        title: 'About Task',
        body: 'Information about the task',
        goTo: 'AboutTask'
    },
    {
        id: 2,
        title: 'Data Format',
        body: 'Data format description',
        goTo: 'DataFormat'
    },
    {
        id: 3,
        title: 'Lorem Ipsum1',
        body: 'Lorem Ipsum short description',
    },
    {
        id: 4,
        title: 'Lorem Ipsum2',
        body: 'Lorem Ipsum short description'
    },
    {
        id: 5,
        title: 'Lorem Ipsum3',
        body: 'Lorem Ipsum short description'
    },
    {
        id: 6,
        title: 'Lorem Ipsum4',
        body: 'Lorem Ipsum short description'
    },
];


export default class HomeScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.itemWrapper}>
                    <FlatList
                        data={DATA}
                        renderItem={({ item }) => <MenuItem title={item.title} body={item.body} onPress={() => item.goTo && this.props.navigation.navigate(item.goTo)} />}
                        keyExtractor={item => item.id}
                        ListHeaderComponent={<View style={{ backgroundColor: 'white', height: 200 }} />}
                        ListFooterComponent={<View style={{ marginBottom: 16 }} />}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                <Image
                    style={styles.imageStyle}
                    source={require('../../Images/header.png')}
                    resizeMode='stretch'
                />
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemWrapper: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center',
    },
    imageStyle: {
        width: '100%',
        height: 200,
        position: 'absolute',
        backgroundColor: 'transparent'
    }
});
