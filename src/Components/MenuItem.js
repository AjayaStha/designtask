//this component is used to display items in homescreen
import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback } from 'react-native';

import { Colors } from '../Constants';

const MenuItem = ({ title, body, onPress }) => (
    <TouchableWithoutFeedback
        onPress={onPress}
    >
        <View style={styles.container}>
            <Text style={[styles.textStyle, { fontWeight: 'bold', fontSize: 24 }]}>{title}</Text>
            <Text style={[styles.textStyle, { fontSize: 20 }]}>{body}</Text>
        </View>
    </TouchableWithoutFeedback>
)

const styles = StyleSheet.create({
    container: {
        marginTop: 16,
        backgroundColor: Colors.color_5eadff,
        flex: 1,
        width: Dimensions.get('window').width - 60,
        paddingVertical: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        shadowColor: '#000000',
        shadowOffset: {
            width: 1,
            height: 5
        },
        shadowRadius: 3,
        shadowOpacity: 0.8,
        elevation: 5
    },
    textStyle: {
        color: 'white',
    }
})

export default (MenuItem);
