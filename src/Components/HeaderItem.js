//this component is used to display header
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { Colors } from '../Constants';

const HeaderItem = ({ title }) => (
    <View style={styles.container}>
        <Text style={styles.textStyle}>{title}</Text>
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 3,
        backgroundColor: Colors.color_6cb7ff,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        color: Colors.color_1d3447,
        fontWeight: 'bold',
        fontSize: 52
    }
})

export default (HeaderItem);
