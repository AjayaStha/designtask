import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen, AboutTaskScreen, DataFormatScreen } from './Screens';

const Stack = createStackNavigator();

const App = () => {
  return (
    <>
      <StatusBar hidden />
      <SafeAreaView style={{ flex: 1 }}>
        <NavigationContainer>
          <Stack.Navigator headerMode='none' initialRouteName="Home">
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="AboutTask" component={AboutTaskScreen} />
            <Stack.Screen name="DataFormat" component={DataFormatScreen} />
          </Stack.Navigator>
        </NavigationContainer>

      </SafeAreaView>
    </>
  );
};

export default App;
